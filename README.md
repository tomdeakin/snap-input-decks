
Test A
------

64x64x64 grid, S8, 32 energy groups.
Estimated memory: 11 GB

Inputs provided are for 32 total threads.
 MPI  OMP  Total   Shape
 32    1    32      8 x 16
  4    8    32     32 x 32
  1   32    32     64 x 64

 20    1    20     12 x 16 (grid now 64x60x64)
 20    4    80     12 x 16 (grid now 64x60x64)
 20    8   160     12 x 16 (grid now 64x60x64)
  4    5    20     32 x 32
  4   10    40     32 x 32

Test B
------

1024x16x16 grid, S8, 32 energy groups.
Estimated memory: 11 GB.

 MPI  OMP  Total   Shape
 32    1    32      2 x 4
  4    8    32      8 x 8
  1   32    32     16 x 16

 64    1    64      2 x 2
 64    2   128      2 x 2
 64    4   256      2 x 2

 20    1    20      4 x 4 (grid now 1024x20x16)
 20    4    20      4 x 4 (grid now 1024x20x16)
  4    5    20      8 x 8

Test C
------

1024x4x4 grid, S32, 32 energy groups.
Estimated memory: 9.2 GB.

 MPI  OMP  Total   Shape
  16   2    32      1 x 1
   4   8    32      2 x 2
   1  32    32      4 x 4

   4  16    64      2 x 2
   4  32   128      2 x 2
   4  64   256      2 x 2

   4   5    20      2 x 2
   4  10    40      2 x 2
   4  20    80      2 x 2


Test D
------

1024*16*16 grid, S32, 32 energy groups.
Estimated memory: 150 GB

 MPI  OMP  Total   Shape
  32   1    32      2 x 4
  16   2    32      4 x 4
   4   8    32      8 x 8

   4   5    20      8 x 8
   4  10    40      8 x 8
   4  20    80      8 x 8

